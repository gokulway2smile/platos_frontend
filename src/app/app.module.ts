import { BrowserModule,DomSanitizer} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {NgPipesModule} from 'ngx-pipes';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { AmazingTimePickerModule } from 'amazing-time-picker'; 
import { NgxCarouselModule } from 'ngx-carousel';
import 'hammerjs';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';




import { Routes, RouterModule } from '@angular/router';

import { FormsModule } from '@angular/forms';
import { Http, HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component'
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import { VendorComponent } from './vendor/vendor.component';
import { AddMenuComponent } from './add-menu/add-menu.component';
import { AuditComponent } from './audit/audit.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { MenuComponent } from './menu/menu.component';
import { MisreportitemComponent } from './misreportitem/misreportitem.component';
import { MisreportorderComponent } from './misreportorder/misreportorder.component';
import { OrdersComponent } from './orders/orders.component';
import { PaymentComponent } from './payment/payment.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { HomeCaterarComponent } from './home-caterar/home-caterar.component';
import {ForgetpasswordComponent} from './forgetpassword/forgetpassword.component';
import {ChangepasswordComponent} from './changepassword/changepassword.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
 

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    VendorComponent,
    AddMenuComponent,
    AuditComponent,
    ConfirmationComponent,
    MenuComponent,
    MisreportitemComponent,
    ChangepasswordComponent,
    MisreportorderComponent,
    OrdersComponent,
    PaymentComponent,
    ForgetpasswordComponent,
    ProfileComponent,
    DashboardComponent,
    SignupComponent,
    LoginComponent,
    HomeCaterarComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AmazingTimePickerModule,
    BsDatepickerModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    DatepickerModule.forRoot(),
    NgbModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    AgGridModule.withComponents([]),
    NgxCarouselModule,
    HttpModule,
    HttpClientModule,
    ToastrModule.forRoot({
        timeOut: 1000,
        positionClass: 'toast-top-right',
        preventDuplicates: true,
     }),
    FormsModule,
    SweetAlert2Module.forRoot({
            buttonsStyling: false,
            customClass: 'modal-content',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn'
        }),
  ],
  exports:[
   
  ],
  providers: [],
  bootstrap: [AppComponent],
  //exports: [BsDropdownModule, TooltipModule, ModalModule]

})

export class AppModule { }
