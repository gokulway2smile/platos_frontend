import { Component, OnInit,ViewChild, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import {
    AuthService
} from '../auth.service';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-changepassword', 
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {

newpassword:any;
confirmpassword:any;
error:any=[];
value:any;
catt_id:any;
rowsFilter:any;
result:any;

  constructor(private router: Router, private _route: ActivatedRoute,private spinnerService: Ng4LoadingSpinnerService,private http: HttpClient,private spinner: NgxSpinnerService,private auth: AuthService) { }

  ngOnInit() {
  }

  onSubmit()
     {
     	this.error = "";   

        
        if (!this.newpassword) {
            this.error = "Please Enter New Password";
            return;
        }
          if (!this.confirmpassword) {
            this.error = "Please Enter Confirm Password ";
            return;
        }
         if (this.confirmpassword != this.newpassword ) {
            this.error = "Password Doesn't Match";
            return;
        }

          let myItem = this.auth.getToken();
          this.value = { "id" : myItem}

          this.spinner.show();  
          this.spinnerService.show();
          this.http.post(AppSettings.API_ENDPOINT + 'GetCatter_details/', this.value).subscribe((data: any) => {          
          this.rowsFilter = data.data;
          this.catt_id =data.data.catt_id;


                let data1 = {
                    "new_password": this.newpassword,
                    "confirm_password": this.confirmpassword,
                    "token" : myItem
                } 
            

            this.http.post(AppSettings.API_ENDPOINT + 'Caterer_resetpassword', {"data" :data1} ).subscribe(data => {
            this.spinner.hide();  
            this.spinnerService.hide();
            this.result = data;   
            
            if (this.result.status == "ok") {
                    Swal("Sucess","Passsword Updated", "success");
                    // this.ShowForm = !this.ShowForm;
                    // this.loaddata();
                    this.router.navigate(['/dashboard']);
                    
                    return;
                } else {
                Swal("Failure", "Not updated", "error");
                // this.ShowForm = !this.ShowForm;
                // this.loaddata();
            }

                               
	            });
         
           });
               
        


        }

}
