import { Component, OnInit,ViewChild, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import {
    AuthService
} from '../auth.service';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import Swal from 'sweetalert2'

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-forgetpassword', 
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.scss']
})
export class ForgetpasswordComponent implements OnInit {

username:any;
error:any=[];
result:any;

  constructor(public toastr: ToastrService,private router: Router, private spinnerService: Ng4LoadingSpinnerService,private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService,private auth: AuthService) { }

  ngOnInit() {
  }

  onSubmit()
     {
     	this.error = "";   

        
        if (!this.username) {
            this.error = "Please Enter Email Address";
            return;
        }
        

                let data1 = {
                    "email": this.username,
                } 
                
            
            this.spinnerService.show();
            this.http.post(AppSettings.API_ENDPOINT + 'Caterer_Forgot', {"data" :data1} ).subscribe(dat => {
            this.spinnerService.hide();
            this.result = dat;   
            
            if (this.result.status == "ok") {

              this.toastr.success("", "New Passsword Sent to your Email");

                    // Swal("Sucess"," ", "success");
                    // this.ShowForm = !this.ShowForm;
                    // this.loaddata();
                    this.router.navigate(['/login']);
                    
                    return;
                } else {
                  this.toastr.warning("", this.result.data);
                // Swal("Failure", "Not Added", "error");
                // this.ShowForm = !this.ShowForm;
                // this.loaddata();
            }

                               
	            });

               
        


        }

}
