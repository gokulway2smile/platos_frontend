import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaterarComponent } from './caterar.component';

describe('CaterarComponent', () => {
  let component: CaterarComponent;
  let fixture: ComponentFixture<CaterarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaterarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaterarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
