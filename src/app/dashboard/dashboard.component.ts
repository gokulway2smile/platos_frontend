import { Component, OnInit,ViewChild,EventEmitter,Output,Input, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';

import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


import {
    AuthService
} from '../auth.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

	rowsFilter:any;
	catt_id;any;
	menu_images:any;
	order_list:any;
	value:any;
	total_order:any;	
	confirmation_pending_count:any = 0;
	confirmed_count:any =  0;
	Out_to_delivery_count:any =0;
	Delivered_count:any =0;
	Cancelled_count:any=0;

  constructor(private router: Router,  private spinnerService: Ng4LoadingSpinnerService, private route: ActivatedRoute, private auth:AuthService,private http: HttpClient,private spinner: NgxSpinnerService) { }

  ngOnInit() {
  	this.loaddata();

  }

  loaddata()
  {
      let myItem = this.auth.getToken();
     this.value = { "token" : myItem}
         this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'GetCatter_details/', this.value).subscribe((data: any) => {          
          this.rowsFilter = data.data;
          this.catt_id =data.data.catt_id;
           
          this.http.post(AppSettings.API_ENDPOINT + 'GetMyMIS_reports/' ,this.value).subscribe((data: any) => {          
          this.spinnerService.hide();
          this.order_list = data.data;
          this.menu_images = AppSettings.Menu_image;         
          this.total_order = this.order_list.length;
          for (var i = this.order_list.length - 1; i >= 0; i--) {
          		
          	if(this.order_list[i].ord_status  == 'Order_placed')
          	{
          		this.confirmation_pending_count = this.confirmation_pending_count + 1;
          	}

          	if(this.order_list[i].ord_status  == 'Confirmed')
          	{
          		this.confirmed_count = this.confirmed_count + 1;
          	}

          	if(this.order_list[i].ord_status  == 'Out_to_delivery')
          	{
          		this.Out_to_delivery_count = this.Out_to_delivery_count + 1;
          	}

          		if(this.order_list[i].ord_status  == 'Delivered')
          	{
          		this.Delivered_count = this.Delivered_count + 1;
          	}

          		if(this.order_list[i].ord_status  == 'Cancelled')
          	{
          		this.Cancelled_count = this.Cancelled_count + 1;
          	}


          }
        });


     });

  }


}
