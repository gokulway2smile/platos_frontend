import { Component, OnInit,ViewChild, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import { ToastrService } from 'ngx-toastr';

import Swal from 'sweetalert2'
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

	DeliveryTime_from :any=[];
	DeliveryTime_to:any=[];
	FirstName:any;
	SurName:any;
	error:any;
	MobileNo:any;
	Password:any;
	DeliveryPincode:any;
	LeadTime:any;
	maxOrder:any;
	MinOrder:any;
	myFiles:any=[];
	EventType:any="";
	FoodCertificate:any;
	EmailAddress:any;
	Scores:any;
    is_edit:boolean =false;
	FileUpload:any;
	GSTCertificate:any;
    Event_list:any=[];
    myFiles1:any=[];
    myFiles2:any=[];
    fileName:any;
    filePath:any;
    fileData:any;
    fileName1:any;
    editdata:any;
    filePath1:any;
    fileData1:any;
    fileName2:any;
    filePath2:any;
    image:any=[];
    fileData2:any;
    selected = null;
    is_upload:boolean=false;
    GST_image:any;
    Food_image:any;
    filesize:any;
    Audit_image:any;

Address = [{
      address:"",
      pincode:"",
    }];

    fieldArray: Array<any> = [];
    newAttribute: any = {};
    isEditItems: boolean;


  constructor(public toastr: ToastrService,private router: Router,private spinnerService: Ng4LoadingSpinnerService, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) { }

  ngOnInit() {

      this.spinner.show();        
     this.spinnerService.show();
    this.http.get(AppSettings.API_ENDPOINT + 'getall_events/').subscribe((data: any) => {  
      this.spinnerService.hide();
     this.spinner.hide();        
     
        this.Event_list = data.data;
        // this.AllData    = [];
    })

  }



getFileDetails (e) {
  //console.log (e.target.files);
  for (var i = 0; i < e.target.files.length; i++) {
    this.myFiles.push(e.target.files[i]);
    this.is_upload=true;
  }
}

getFileDetails1 (e) {
  //console.log (e.target.files);
  for (var i = 0; i < e.target.files.length; i++) {
    this.myFiles1.push(e.target.files[i]);
    this.is_upload=true;
  }
}

getFileDetails2 (e) {
  //console.log (e.target.files);
  for (var i = 0; i < e.target.files.length; i++) {
    this.myFiles2.push(e.target.files[i]);
    this.is_upload=true;
  }
}



onSubmit()
     {     
       console.log("ciming")
         this.error = "";   
         this.image =[];       
        if (!this.FirstName) {
            this.error = "Please Enter FirstName";
            return;
        }
       
        if (!this.MobileNo) {
            this.error = "Please Enter MobileNo";
            return;
        }
        if (!this.Password) {
            this.error = "Please Enter Password";
            return;
        }
  
        if (!this.LeadTime) {
            this.error = "Please Enter Lead Time";
            return;
        }
        if (!this.DeliveryTime_from) {
            this.error = "Please Enter Delivery From Time";
            return;
        }
        if (!this.DeliveryTime_to) {
            this.error = "Please Enter Delivery To Time";
            return;
        }


        if(!this.is_edit)
        {
           const frmData = new FormData();
           if(this.myFiles.length>0)
           {
           for (var i = 0; i < this.myFiles.length; i++) {
             frmData.append("gstpic", this.myFiles[i]);
           }
           }

            const frmData1 = new FormData();
            if(this.myFiles)
            {
             if(this.myFiles1.length>0)
               {

               for (var i = 0; i < this.myFiles1.length; i++) {
                 frmData1.append("foodpic", this.myFiles1[i]);
               }
               }
           }

               const frmData2 = new FormData();
               if(this.myFiles2)
            {
                if(this.myFiles2.length>0)
               {
               for (var i = 0; i < this.myFiles2.length; i++) {
                 frmData2.append("filepic", this.myFiles2[i]);
               }
               }
           }

           let data1;
           
          
                if(this.myFiles.length>0)
                {
                   let temp_data:any;
                    let data = [];
                    this.fileName = null;
                    this.filePath = null;    
                    this.spinnerService.show();    
                    this.http.post(AppSettings.API_ENDPOINT + 'catgst_image/', frmData).subscribe(async(temp_data: any) => {
                    this.spinnerService.hide();
                    this.fileData = temp_data.data;
                    this.fileName = this.fileData.gstpic;
                    this.filePath = this.fileData.file_path; 
                    this.GST_image = this.fileName;
                    // await this.image.thispush({'GST':this.fileName});

                    })
                    }


 
                if(this.myFiles1.length>0)
                {
               let temp_data1:any;
                this.fileName1 = null;
                this.filePath1 = null;        
                this.spinnerService.show();
                this.http.post(AppSettings.API_ENDPOINT + 'catfood_image/', frmData1).subscribe(async(temp_data1: any) => {
                this.spinnerService.hide();
                this.fileData1 = temp_data1.data;
                this.fileName1 = this.fileData1.foodpic;
                this.filePath1 = this.fileData1.file_path;
                
                this.Food_image = this.fileName1;
                
                // await this.image.push({'Food':this.fileName1});
                    });
                }
                
                   if(this.myFiles1.length>0)
                {

               let temp_data2:any;
                this.fileName2 = null;
                this.filePath2 = null;
                this.spinnerService.show();        
                this.http.post(AppSettings.API_ENDPOINT + 'catfile_image/', frmData2).subscribe(async(temp_data2: any) => {
                this.spinnerService.hide();
                this.fileData2 = temp_data2.data;
                this.fileName2 = this.fileData2.filepic;
                this.filePath2 = this.fileData2.file_path;
                this.filesize  = this.fileData2.filesize;
                this.Audit_image = this.fileName2;

               await this.image.push({'upload':this.fileName2});

               });


                }                
             
                this.spinnerService.show();
                setTimeout(()=> this.apihit(),3000)
                this.spinnerService.hide();           
                
        }
        else
        {
           const frmData = new FormData();
           let data1;
           if(this.myFiles.length>0)
           {
           for (var i = 0; i < this.myFiles.length; i++) {
             frmData.append("pic", this.myFiles[i]);
           }
                let temp_data:any;
                this.fileName = null;
                this.filePath = null;        
                this.spinnerService.show();
                this.http.post(AppSettings.API_ENDPOINT + 'catgst_image/', frmData).subscribe((temp_data: any) => {
                  this.spinnerService.hide();
                this.fileData = temp_data.data;
                this.fileName = this.fileData.pic;
                this.filePath = this.fileData.file_path;
                

              if(this.myFiles1.length>0)
               {
               for (var i = 0; i < this.myFiles1.length; i++) {
                 frmData.append("foodpic", this.myFiles1[i]);
               }
               }

               let temp_data1:any;
                this.fileName1 = null;
                this.filePath1 = null;        
                this.spinnerService.show();
                this.http.post(AppSettings.API_ENDPOINT + 'catfood_image/', frmData).subscribe((temp_data1: any) => {
                this.spinnerService.hide();
                this.fileData1 = temp_data1.data;
                this.fileName1 = this.fileData1.foodpic;
                this.filePath1 = this.fileData1.file_path;


                if(this.myFiles2.length>0)
               {
               for (var i = 0; i < this.myFiles2.length; i++) {
                 frmData.append("filepic", this.myFiles2[i]);
               }
               }

               let temp_data2:any;
                this.fileName2 = null;
                this.filePath2 = null;
                this.spinnerService.show();        
                this.http.post(AppSettings.API_ENDPOINT + 'catfile_image/', frmData).subscribe((temp_data2: any) => {
                this.spinnerService.hide();
                this.fileData2 = temp_data2.data;
                this.fileName2 = this.fileData2.filepic;
                this.filePath2 = this.fileData2.file_path;
                this.filesize  = this.fileData2.filesize;


                 data1 = {                    
                   "catt_first_name": this.FirstName,
                    "catt_sur_name": this.SurName,
                    "catt_email_address": this.EmailAddress,
                    "catt_mobile_no": this.MobileNo,
                    "catt_password": this.Password,
                    "catt_address" : this.Address,
                    // "catt_delivery_pincode": this.DeliveryPincode,
                    "catt_lead_time": this.LeadTime,
                    "DeliveryTime_from": this.DeliveryTime_from,
                    "DeliveryTime_to": this.DeliveryTime_to,
                    "catt_min_order" : this.MinOrder,
                    "catt_max_order" : this.maxOrder,
                    "catt_event_type" : this.EventType,
                    "catt_GST_certificate" : this.fileName,
                    "catt_food_certificate" :this.fileName1,
                    "catt_score" :this.Scores,
                    "catt_file_upload":this.fileName2,
                    "audit_filesize" : this.filesize
                    
                } 
                
                    this.updateone(data1);
                 });
                 });
                 });

                }
                else
                {
                     data1 = {
                    "catt_first_name": this.FirstName,
                    "catt_sur_name": this.SurName,
                    "catt_email_address": this.EmailAddress,
                    "catt_mobile_no": this.MobileNo,
                    "catt_password": this.Password,
                    "catt_address" : this.Address,
                    // "catt_delivery_pincode": this.DeliveryPincode,
                    "catt_lead_time": this.LeadTime,
                    "DeliveryTime_from": this.DeliveryTime_from,
                    "DeliveryTime_to": this.DeliveryTime_to,
                    "catt_min_order" : this.MinOrder,
                    "catt_max_order" : this.maxOrder,
                    "catt_event_type" : this.EventType,
                    "catt_GST_certificate" : this.GSTCertificate,
                    "catt_food_certificate" :this.FoodCertificate,
                    "catt_score" :this.Scores,
                    "catt_file_upload":this.FileUpload
                    
                                   
                }  
                   this.updateone(data1);
                }

        }



        }


        apihit(){
          let fromtime= this.DeliveryTime_from;
               let totime= this.DeliveryTime_to;
                let data1 = {
                    "catt_first_name": this.FirstName,
                    "catt_sur_name": this.SurName,
                    "catt_email_address": this.EmailAddress,
                    "catt_mobile_no": this.MobileNo,
                    "catt_password": this.Password,
                    "catt_lead_time": this.LeadTime,
                    "catt_address" : this.Address,
                    "DeliveryTime_from": fromtime,
                    "DeliveryTime_to": totime,
                    "catt_min_order" : this.MinOrder,
                    "catt_max_order" : this.maxOrder,
                    "catt_event_type" : this.EventType,
                    "GST_image" : this.GST_image,
                    "Food_image" : this.Food_image,
                    "Audit_image" : this.Audit_image,
                    "catt_score" :this.Scores  ,
                    "audit_filesize" : this.filesize                 
                    
                } 

                console.log(data1);
                this.addnew(data1);
        }

        updateone(data1)
   {
           
           this.spinnerService.show();
           this.http.put(AppSettings.API_ENDPOINT + 'getall_catterlist/' + this.editdata.catt_id, {
               "data": data1,
           }).subscribe((data: any) => {           
           this.spinnerService.hide();
               if (data.status == "ok") {
                   // Swal("Success", " Caterer Updated", "success");
                   this.toastr.success("", "Caterer Updated");

                   // this.ShowForm = !this.ShowForm;
                   // this.loaddata();
                   return;
               } else {
               Swal("Failure", " Not Updated", "error");
               // this.ShowForm = !this.ShowForm;
               // this.loaddata();
           }
           }, error => {})

   }



addItem(index)
  {
      

    if (this.fieldArray.length <= 4) {
      this.fieldArray.push(this.newAttribute);
      this.newAttribute = {};
    } else {

    }
    this.Address.push({
      address:this.Address[0].address,
      pincode :this.Address[0].pincode
    });

  
  }


 addnew(data1)
    {
      
        this.spinner.show(); 
        this.spinnerService.show(); 
        this.http.post(AppSettings.API_ENDPOINT + 'getall_catterlist/', {
                "data": data1,
            }).subscribe((data: any) => {
        this.spinner.show();  
        this.spinnerService.hide();
                if (data.status == "ok") {
                    // Swal("Success", "Caterer ", "success");
                    this.toastr.success("", "Caterer Added");
                    // this.ShowForm = !this.ShowForm;
                    // this.loaddata();
                    this.router.navigate(['/login']);
                    
                    return;
                } else {
                  this.toastr.warning("", data.data);
                // Swal("Failure", "Not Added", "error");
                // this.ShowForm = !this.ShowForm;
                // this.loaddata();
            }
            }, error => {})
        

}

deleteFieldValue(index) {
    if(this.Address.length>1)
    {
       this.Address.splice(index, 1);     
    }
   
 }

 onEditCloseItems() {
    this.isEditItems = !this.isEditItems;
  }



}