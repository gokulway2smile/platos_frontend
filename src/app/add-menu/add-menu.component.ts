import { Component, OnInit,ViewChild, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import { ToastrService } from 'ngx-toastr';

import {
    AuthService
} from '../auth.service';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.scss']
})
export class AddMenuComponent implements OnInit {

  constructor(public toastr: ToastrService,private router: Router, private spinnerService: Ng4LoadingSpinnerService, private route: ActivatedRoute, private auth:AuthService,private http: HttpClient,private spinner: NgxSpinnerService) { }

  cusinis_list:any=[];
  group_list:any=[];
  package_list:any=[];
  mealtype_list:any=[];
  dropdownSettings1:any;
  package_id:any="";
  Cuisine_id:any="";
  Mealtype_id:any="";
  group_id:any="";
  is_veg:any="1";
  myFiles:any=[];
  dropdownList:any=[];
  filePath:any;
  fileName:any=[];
  fileData:any;
  Dish_Title:any;
  dish_description:any;
  dish_description1:any;
  minorder:any;
  discount:any;
  discount_duration:any;
  maxorder:any;
  minorder1:any;
  selectedItems1:any=[];
  maxorder1:any;
  price_person:any;
  is_special:any;
  price_person1:any;
  catt_id:any;
  value:any;
  myFiles1:any=[];
  edit:any;
  ids:any;
  Image_Base:any = AppSettings.Image_Base;
  menu_id:any;
  is_upload:boolean=false;
  
  Menu_group:any;
  ngOnInit() {
  	 this.loaddata();
  //   this.showSuccess();
    
  }





  urls = [];
  onSelectFile(event) {
    this.is_upload=true;
    if (event.target.files && event.target.files[0]) {
        var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                this.myFiles.push(event.target.files[i]);

                reader.onload = (event:any) => {
                   this.urls.push(event.target.result); 
                }

                reader.readAsDataURL(event.target.files[i]);
        }
    }
  }

bulk(event)
{
if (event.target.files && event.target.files[0]) {
        var excel = event.target.files.length;
        for (let i = 0; i < excel; i++) {
                var reader = new FileReader();

                this.myFiles1.push(event.target.files[i]);

        }
    }
    const frmData1 = new FormData();
    if(this.myFiles1.length>0)
         {
         for (var i = 0; i < this.myFiles1.length; i++) {
           frmData1.append("import_file", this.myFiles1[i]);
           frmData1.append('catt_id',this.catt_id);
         }
    
    this.spinnerService.show();
    this.http.post(AppSettings.API_ENDPOINT + 'bulk_importmenu/', frmData1).subscribe((temp_data: any) => {
    this.spinnerService.hide();
            if (temp_data.status == "ok") {
                  this.toastr.success("", "Menu Bulk Imported");
                  return;
                } else {
                this.toastr.warning("", temp_data.data);
                this.loaddata();                
            }
            }, error => {})

  }
}
 addMenu()
 {



 	const frmData = new FormData();
 	if(this.myFiles.length>0)
           {
           for (var i = 0; i < this.myFiles.length; i++) {
             frmData.append("pic[]", this.myFiles[i]);
           }
           }
           
   // if(this.myFiles.length>0)
   //  {
        let temp_data:any;
        this.fileName=[];
        this.filePath = null;        
        this.spinnerService.show();
        this.http.post(AppSettings.API_ENDPOINT + 'AddMenu_images/', frmData).subscribe((temp_data: any) => {
          this.spinnerService.hide();

        if(temp_data)
        {
        	this.fileData = temp_data.data;
            this.fileName.push(this.fileData);
      	}
      	else
      	{
           
           this.fileName =[];
      	}   

      		let data1 = {
                    "menu_name": this.price_person1,
                    "menu_description": this.dish_description1,
                    // "min_order": this.minorder1,
                    // "max_order": this.maxorder1,
                    "is_veg": this.is_veg,
                    "meal_type_id": this.Mealtype_id,
                    "cuisine_type_id" : this.Cuisine_id,
                    "package_type_id": this.package_id,
                    // "group_id": this.group_id,
                    "per_person_cost" : this.price_person,
                    "discount": this.discount,
                    "discount_duration": this.discount_duration,
                    "is_recommended" : this.is_special,
                    // "catt_id" : this.EventType
                    "menu_images": this.fileName,
                    'catt_id' : this.catt_id,
                    "item_group":this.selectedItems1

                }
                if(this.menu_id)
                {
                    this.update(data1,this.menu_id);
                }
                else
                {
                   this.addnew(data1);             
                }
     
   });  
  
}

  addnew(data1)
    {
        this.spinnerService.show();
        this.http.post(AppSettings.API_ENDPOINT + 'getAll_menu/',{
                "data": data1,
            }).subscribe((data: any) => {
        this.spinnerService.hide();
                if (data.status == "ok") {
                  this.toastr.success("", "Menu Added");
                  this.router.navigate(['menus']);
                  return;
                } else {
                this.toastr.warning("", data.data);
                this.loaddata();                
            }
            }, error => {})
        
    }

    update(data1,id)
    {
        this.spinnerService.show();
        this.http.put(AppSettings.API_ENDPOINT + 'getAll_menu/' + id, {
                "data": data1,
            }).subscribe((data: any) => {
        this.spinnerService.hide();
                if (data.status == "ok") {
                  this.toastr.success("", "Menu Updated");
                  this.router.navigate(['menu'])
                   return;
                } else {
                this.toastr.warning("", data.data);
                this.loaddata();
               
            }
            }, error => {})
        
    }

cancel()
{
  this.router.navigate(['menu'])
}
delete_img(id)
{
  Swal({
  title: 'Are you sure?',
  type: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes!',
  cancelButtonText: 'No'
}).then((result) => {
  if (result.value) {
      this.spinnerService.show();
      this.http.delete(AppSettings.API_ENDPOINT + 'Deletemenu_img/' +id).subscribe((data: any) => {
           if (data.status == "ok") {  
   this.loaddata();      
   this.spinnerService.hide();  
    this.toastr.success("","Menu Image Deleted")
     }
})
   } else if (result.dismiss === Swal.DismissReason.cancel) {
    
  }
})
this.loaddata();


}


reseting()
{

  this.price_person1="";
  this.dish_description="";
  // this.minorder1="";
  // this.maxorder1="";
  this.price_person="";
  this.is_special="";
  this.discount="";
  this.discount_duration="";
  this.Mealtype_id="";
  this.Cuisine_id="";
  this.group_id="";
  this.package_id="";
}

goLive()
{
   this.spinnerService.show();
     let myItem = this.auth.getToken();
     this.value = { "token" : myItem}

        this.http.put(AppSettings.API_ENDPOINT + 'caterer_menulive/' , {
                "token": myItem,
            }).subscribe((data: any) => {
        this.spinnerService.hide();
                if (data.status == "ok") {

                }
              })

}

  loaddata()
  {
     	        
    this.spinnerService.show();
    this.http.get(AppSettings.API_ENDPOINT + 'getall_cuisine/').subscribe((data: any) => {       
     this.spinnerService.hide();
        this.cusinis_list = data.data;
        // this.AllData    = [];
    })

this.http.get(AppSettings.API_ENDPOINT + 'getall_groups/').subscribe((data: any) => {  
     this.spinner.hide();        
     this.spinnerService.hide();
        this.group_list = data.data;
        // this.AllData    = [];

        let dropdown:any=[];
        for (var i = data.data.length - 1; i >= 0; i--) {
          dropdown.push({"menugroup_id":data.data[i].menugroup_id,"menugroup_name":data.data[i].menugroup_name})

        }

         this.dropdownList = dropdown;

         this.dropdownSettings1 = {
              singleSelection: false,
              idField: 'menugroup_id',
              textField: 'menugroup_name',
              selectAllText: 'Select All',
              unSelectAllText: 'UnSelect All',
              itemsShowLimit: 5,
              allowSearchFilter: true
            };



    })

  
 




this.http.get(AppSettings.API_ENDPOINT + 'getall_package/').subscribe((data: any) => {  
     this.spinner.hide();        
     this.spinnerService.hide();
        this.package_list = data.data;
        // this.AllData    = [];
    })

this.http.get(AppSettings.API_ENDPOINT + 'getall_mealtypes/').subscribe((data: any) => {  
     this.spinner.hide();        
     this.spinnerService.hide();
        this.mealtype_list = data.data;
        // this.AllData    = [];
    })
	  


   let myItem = this.auth.getToken();
     this.value = { "token" : myItem}
         this.http.post(AppSettings.API_ENDPOINT + 'GetCatter_details/', this.value).subscribe((data: any) => {          
         // this.rowsFilter = data.data;
         this.spinnerService.hide();
         this.catt_id =data.data.catt_id;

     });

    



    this.route.queryParams.subscribe(params => {
      
            if(params.value)
            {

          console.log(params)
                  
        this.spinnerService.show();
         this.http.get(AppSettings.API_ENDPOINT + 'getMenu_item/' +params.value).subscribe((data: any) => {          
         // this.rowsFilter = data.data;
         this.spinnerService.hide();
         let data1 = data.data;

  this.price_person1=data1.menu_name;
  this.dish_description1=data1.menu_description;
  // this.minorder1=data1.min_order;
  // this.maxorder1=data1.max_order;
  this.price_person=data1.per_person_cost;
  this.discount=data1.discount;
  this.discount_duration=data1.per_discount_duration;
  this.is_special=data1.is_recommended;
  this.Mealtype_id=data1.meal_type_id;
  this.Cuisine_id=data1.cuisine_type_id;
  // this.group_id=data1.group_id
  this.discount= data1.discount_off;
  this.discount_duration= data1.discount_duration;

  this.package_id=data1.package_type_id;
  this.edit = true;
  this.menu_id = data1.menu_id;


let img=[];
let img_id=[];
data1.images.forEach(function (value) {
  img.push(AppSettings.Menu_image+value.menu_imgname)
  img_id.push(value.menu_imgid)
  
  
}); 

if(!this.is_upload)
{
  this.urls =img;
  this.ids =img_id;
}


     });

     this.spinnerService.show();
     this.http.post(AppSettings.API_ENDPOINT + 'getcat_grouplist/',this.value).subscribe((data: any) => {
     this.spinnerService.hide();
     this.Menu_group = data.data;   
     

     if(this.Menu_group.length>0)
     {
       let count = this.Menu_group[0].menu_group.length;
       for (var i = 0; i < count; ++i) {            
      if(this.Menu_group[0].menugroup_id ==this.Menu_group[0].menu_group[i].menugroup_id && this.Menu_group[0].menu_group[i].menu_id == params.value)
      this.selectedItems1.push({ menugroup_id: this.Menu_group[0].menugroup_id, menugroup_name: this.Menu_group[0].menugroup_name});                
  }
  }
})    
     console.log(this.selectedItems1)

            }

            else
            {



  this.price_person1="";
  this.dish_description="";
  // this.minorder1="";
  // this.maxorder1="";
   this.discount="";
  this.discount_duration="";
  this.price_person="";
  this.is_special="";
  this.Mealtype_id="";
  this.Cuisine_id="";
  this.group_id="";
  this.package_id="";


            }


          });




  }
  


}


