import { Component, OnInit,ViewChild, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


import { ToastrService } from 'ngx-toastr';

import {
    AuthService
} from '../auth.service';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-audit',
  templateUrl: './audit.component.html',
  styleUrls: ['./audit.component.scss']
})
export class AuditComponent implements OnInit {

  constructor(public toastr: ToastrService,private router: Router, private route: ActivatedRoute,private spinnerService: Ng4LoadingSpinnerService, private auth:AuthService,private http: HttpClient,private spinner: NgxSpinnerService) { }
value:any;
rowsFilter:any;
audit:any;
catt_id:any;
Audit_score:any;
first_audit:any={};
seAddArtist:any;
audit_files:any;
Audit_file = AppSettings.Audit_file;
  ngOnInit() {

  	this.loaddata();
  }



loaddata()
{
	  let myItem = this.auth.getToken();
     this.value = { "token" : myItem}
     this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'GetCatter_details/', this.value).subscribe((data: any) => {   
         this.spinnerService.hide();       
          this.rowsFilter = data.data;
          this.audit =data.data.audit;
         console.log(this.rowsFilter)
          this.first_audit = data.data.audit[0];
         this.Audit_score = data.data.catt_score;
         this.catt_id =data.data.catt_id;
         this.audit_files=  AppSettings.Audit_file
     });
}


Request_audit()
{
  let myItem = this.auth.getToken();
     this.value = { "token" : myItem}
     this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'Requesting_audit/', this.value).subscribe((data: any) => {   
         this.spinnerService.hide();       

          if (data.status == "ok") {
                    this.toastr.success("","Audit Request Send");
                    this.ngOnInit();                                       
                   return;
                } else {
                  this.toastr.warning("",data.data);                
            }

  });

}
download(audit)
  {
    

    window.open(AppSettings.Audit_file+audit)
      
      // this.audit =audit;
      // console.log(this.audit);
      // for (var i = this.audit.length - 1; i >= 0; i--) {
        
      //   if(this.audit[i].audit_id ==  id)
      //   {
      //        console.log(AppSettings.Audit_file+this.audit[i].audit_file);
      //        // this.router.navigate([AppSettings.Audit_file+this.audit[i].audit_file])

      //   }
        

      // }

  }


}
