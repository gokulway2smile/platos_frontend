import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisreportitemComponent } from './misreportitem.component';

describe('MisreportitemComponent', () => {
  let component: MisreportitemComponent;
  let fixture: ComponentFixture<MisreportitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisreportitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisreportitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
