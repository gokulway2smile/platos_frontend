import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import * as moment from "moment";


export interface Credentials {
  // Customize received credentials here
  access_token: string;
  token_type: string;
  expires_in: number;
  logged_at: Date;
}

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

// const credentialsKey = 'credentials';

/**
 * Provides a base for authentication workflow.
 * The Credentials interface as well as login/logout methods should be replaced with proper implementation.
 */
@Injectable()
export class AuthenticationService {

  private _credentials: Credentials | null;
  private credentialsKey = 'Catter';

    
  constructor() {

   


    const savedCredentials = sessionStorage.getItem(this.credentialsKey) || localStorage.getItem(this.credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  /**
   * Authenticates the user.
   * @param {LoginContext} context The login parameters.
   * @return {Observable<Credentials>} The user credentials.
   */
  login(context: any): Observable<Credentials> {
    this.setCredentials(context);
    return of(context);
  }

  /**
   * Logs out the user and clear credentials.
   * @return {Observable<boolean>} True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.setCredentials();
    return of(true);
  }

  /**
   * Checks is the user is authenticated.
   * @return {boolean} True if the user is authenticated.
   */
  isAuthenticated(): boolean {

    // // return true;

    // return !!this.credentials;


    if(!this.credentials)
   {
     return false;
   }
   else
   {

     try{

       let logged_at = moment(this.credentials.logged_at).add(this.credentials.expires_in, 'seconds');


       if(logged_at > moment(new Date()))
       {
         return true;
       }
       else
       {
         return false;

       }

     }
     catch(e)
     {
       return false;
     }


   }


  }

  /**
   * Gets the user credentials.
   * @return {Credentials} The user credentials or null if the user is not authenticated.
   */
  get credentials(): Credentials | null {
    return this._credentials;
  }

  setNotifications(data)
  {  

        // let oldData:any = this.getNotifications() || [];

        //  data = oldData.concat(data)

        let dataInfo = data || [];

        return localStorage.setItem(this.credentialsKey+"_notifications",JSON.stringify(dataInfo))
  }

  getNotifications()
  {

        return JSON.parse(localStorage.getItem(this.credentialsKey+"_notifications")) || [];
  }

  /**
   * Sets the user credentials.
   * The credentials may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the credentials are only persisted for the current session.
   * @param {Credentials=} credentials The user credentials.
   * @param {boolean=} remember True to remember credentials across sessions.
   */
  private setCredentials(credentials?: any, remember?: boolean) {


    this._credentials = credentials || null;

    if (credentials) {

       credentials.logged_at = new Date().toString();
    
      // const storage = remember ? localStorage : sessionStorage;
      const storage =  localStorage;


      storage.setItem(this.credentialsKey, JSON.stringify(credentials));

    } else {

      localStorage.removeItem(this.credentialsKey);

      sessionStorage.removeItem(this.credentialsKey);
    }
  }

}
