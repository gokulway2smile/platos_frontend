import { Component, OnInit,ViewChild,EventEmitter,Output,Input, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import {
    AuthService
} from '../auth.service';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {


  constructor(private router: Router,  private spinnerService: Ng4LoadingSpinnerService,  private route: ActivatedRoute, private auth:AuthService,private http: HttpClient,private spinner: NgxSpinnerService) { }
value:any;
rowsFilter:any;
Audit_score:any;
catt_id:any;
selectedTab = "tab-present";
present_orders:any=[]
future_orders:any=[]
past_orders:any=[]
menu_images:any;
order_list:any=[];


@Input() rating: number;
@Input() ratingone: number;
@Input() ratingtwo: number;
@Input() itemId: number;
@Input() itemIdone: number;
@Input() itemIdtwo: number;

@Output() ratingClick: EventEmitter<any> = new EventEmitter<any>();
@Output() ratingClickone: EventEmitter<any> = new EventEmitter<any>();
@Output() ratingClicktwo: EventEmitter<any> = new EventEmitter<any>();



 inputName: string;
 inputNameone: string;
 inputNametwo: string;
  ngOnInit() {
  	 this.inputName = this.itemId + '_rating';
  	 this.inputNameone = this.itemIdone + '_ratingone';
  	 this.inputNametwo = this.itemIdtwo + '_ratingtwo';
     this.loaddata();
  }


onTabChange($event: any) {
        this.selectedTab = $event.nextId
        // this.tabletrue = true;
        
        // this.divtoggle = false;
    }

  loaddata()
  {
      let myItem = this.auth.getToken();
     this.value = { "token" : myItem}
     this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'GetCatter_details/', this.value).subscribe((data: any) => {          
           this.spinnerService.hide();
          this.rowsFilter = data.data;
          this.catt_id =data.data.catt_id;
          this.spinnerService.show();
          this.http.post(AppSettings.API_ENDPOINT + 'GetAllMy_Orders/' ,this.value).subscribe((data: any) => {          
            this.spinnerService.hide();
          this.present_orders = data.data.present_orders;
          this.future_orders = data.data.future_orders;
          this.past_orders = data.data.past_orders;
          this.menu_images = AppSettings.Menu_image;         

        });


     });

  }

  Order_stats(status,id)
{
    let data={
        "order_id":id,
        "ord_status":status
    }
       this.spinner.show();  
       this.spinnerService.show();
          this.http.put(AppSettings.API_ENDPOINT + 'GetMyOrder_List/' +id,{"data":
            data}).subscribe((data: any) => {          
           this.spinner.show();  
           this.spinnerService.hide();
                if (data.status == "ok") {
                  Swal("Success", " Order Updated", "success");
                  // this.ShowForm = !this.ShowForm;
                  this.loaddata();
                }
          // this.catt_id =data.data.catt_id;
          
          this.menu_images = AppSettings.Menu_image;         

        });


}
  onClick(rating: number): void {
    this.rating = rating;
    this.ratingClick.emit({
      itemId: this.itemId,
      rating: rating
    });
  }

   click(ratingone: number): void { 
    this.ratingone = ratingone;
    this.ratingClickone.emit({
      itemId: this.itemId,
      ratingClickone: ratingone
    });
  }
   twoclick(ratingtwo: number): void { 
    this.ratingtwo = ratingtwo;
    this.ratingClicktwo.emit({
      itemIdtwo: this.itemIdtwo,
      ratingClicktwo: ratingtwo
    });
  }

}
