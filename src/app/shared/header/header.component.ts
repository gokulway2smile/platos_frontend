import {
    Component,
    NgZone,
    OnInit,
    ViewChild,
    ElementRef,
    HostListener,Input
} from '@angular/core';


import { Observable, of } from 'rxjs';

import { ToastrService } from 'ngx-toastr';

import {
    Router,
    NavigationEnd,ActivatedRoute
} from '@angular/router';
import {
    AuthenticationService
} from '../../authentication/authentication.service';

import { AuthService } from '../../auth.service';

import { AppSettings } from '../../app.setting';

import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
menudropdown:boolean=false;
logged_in:boolean=false;
rowsFilter:any;
value:any;
Caterer_admin:any;
first_name:any;
audit_deactive;any;
 // private _credentials: Credentials | null;
  private credentialsKey = 'ADMIN';

  constructor(private route:ActivatedRoute,
      private toastr: ToastrService,
       private spinnerService: Ng4LoadingSpinnerService,
      public auth: AuthService,
      private router: Router,private http: HttpClient) { }


logout() {
            this.auth.logout();
            this.router.navigate(["/home"], { replaceUrl: true });
            window.location.reload();
        }





  ngOnInit() {

if (this.auth.isAuthenticated()) {

     this.logged_in=true;

     if(this.auth.CheckRole() == 'Caterer_Admin')
          {
             this.Caterer_admin = true
          }
          else if(this.auth.CheckRole() == 'Caterer_Subadmin')
          {
            this.Caterer_admin = false
          }

     console.log(this.Caterer_admin);


    }


 let myItem = this.auth.getToken();
     this.value = { "token" : myItem}
     this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'GetCatter_details/', this.value).subscribe((data: any) => {          
           this.spinnerService.hide();
         this.rowsFilter = data.data; 

          this.first_name= this.rowsFilter.catt_first_name;
          if(this.first_name)
          {
            this.first_name= this.rowsFilter.catt_first_name.toUpperCase();
          }

          this.audit_deactive = this.rowsFilter.audit_deactive
       });


  }

togglemenu(){
this.menudropdown = !this.menudropdown;
}



}
