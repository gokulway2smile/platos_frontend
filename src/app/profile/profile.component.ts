import { Component, OnInit,ViewChild, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import {
    AuthService
} from '../auth.service';

 
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

rowsFilter:any;
value:any;
address:any;

profilelist:any=[];
  constructor(private router: Router,private spinnerService: Ng4LoadingSpinnerService,  private auth:AuthService, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) { }

  ngOnInit() {


this.loadData();

  }


loadData() {

         let myItem = this.auth.getToken();
		     this.value = { "token" : myItem}
         this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'GetCatter_details/', this.value).subscribe((data: any) => {          
         this.spinnerService.hide();
         this.rowsFilter = data.data; 
         this.address =this.rowsFilter.cust;
         console.log(this.rowsFilter)
         let Role = this.auth.CheckRole();
         if(Role =='Caterer_Admin')
         {
           this.profilelist = [
          			{
          			 name: this.rowsFilter.catt_first_name,          			 
          			 phone: this.rowsFilter.catt_mobile_no,
          			 email: this.rowsFilter.catt_email_address,
          			 role: 'Admin'
          			}
        			];                 
          }
          else
          {
            this.profilelist = [
                {
                 name: this.rowsFilter.addi_user.cat_usr_name,                 
                 phone: this.rowsFilter.addi_user.cat_usr_mobile,
                 email: this.rowsFilter.addi_user.cat_usr_email,
                 role: 'Sub Admin'
                }
              ];                 
          
          }

              }, error => {                
              })       
    }


}
