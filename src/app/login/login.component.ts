import { Component, OnInit,ViewChild, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import {
    AuthService
} from '../auth.service';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { ToastrService } from 'ngx-toastr';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

constructor(public toastr: ToastrService,private router: Router, private _route: ActivatedRoute,private spinnerService: Ng4LoadingSpinnerService,private http: HttpClient,private spinner: NgxSpinnerService,private auth: AuthService) { }

username:any;
error:any=[];
password1:any;
result:any;
  ngOnInit() {


  }


  onSubmit()
     {
       
     	this.error = "";   

        
        if (!this.username) {
            this.error = "Please Enter Email Address";
            return;
        }
        if (!this.password1) {
            this.error = "Please Enter Password";
            return;
        }

                let data1 = {
                    "username": this.username,
                    "password": this.password1
                } 
                
                this.spinner.show();  
                this.spinnerService.show();
            this.http.post(AppSettings.API_ENDPOINT + 'Caterer_Login/', {"data" :data1} ).subscribe(data => {
            this.spinner.hide();  
            this.spinnerService.hide();
            this.result = data;   
                               
	            if (this.result) {                
	                if (this.result.status == "error") {
	                    
                      this.toastr.warning("", this.result.data);
	                    return;
	                }
	                if (this.result.data) {             
	                        
// console.log(data)
        //                   this.authenticationService.login(data)
        // .pipe(finalize(() => {
        //   this.loginForm.markAsPristine();
        //   this.isLoading = false;
        // }))
        // .subscribe(credentials => {
        //   this.router.navigate(['/'], { replaceUrl: true });
        // }, error => {

        //   log.debug(`Login error: ${error}`);
        //   this.error = error;

        // });
	                    if (this.result.data) {                       
	                        let token = this.result.data.token.toString();
	                        let tempFlag = this.result.data.IS_ADMIN || false;
	                        let Flag = this.result.data.SUPER_ADMIN || false;
                          // console.log(this.result.data[0])
	                        this.auth.setToken(this.result.data);
	                        
                          if(this.result.data.is_reseted_password == true)                 
                          {
                            this.router.navigate(["/changepassword"], { replaceUrl: true });
                          }
                          else
                          {
                            this.router.navigate(["/dashboard"], { replaceUrl: true });  
                          }
	                        
                           window.location.reload();
	                    }
	                }
	            }
	        }, error => {
	            this.error = 'Connection Error'
	        });

               
        


        }


}
