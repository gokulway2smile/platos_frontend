import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCaterarComponent } from './home-caterar.component';

describe('HomeCaterarComponent', () => {
  let component: HomeCaterarComponent;
  let fixture: ComponentFixture<HomeCaterarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCaterarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCaterarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
