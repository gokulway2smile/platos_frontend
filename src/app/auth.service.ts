import {
    Injectable
} from '@angular/core';
import {
    JwtHelperService
} from '@auth0/angular-jwt';
import {
    Router
} from '@angular/router';
import { Observable, of } from 'rxjs';
import * as moment from "moment";

declare var Fingerprint2: any;


export interface Credentials {
  // Customize received credentials here
  access_token: string;
  token_type: string;
  expires_in: number;
  logged_at: Date;
}


@Injectable()
export class AuthService {
    constructor(public jwtHelper: JwtHelperService, ) {
        // this.setClientToken()

     }

     private _credentials: Credentials | null;
     private credentialsKey = 'TOKEN';


    public getToken(): string {
        return localStorage.getItem('token');
    }
    public getClientToken(): string {
        return localStorage.getItem('client_token');
    }

     public CheckRole(): string {
        return localStorage.getItem('Role');
    }
    

    public setClientToken() {
        let client_data = new Fingerprint2().get(function(result, components) {
            localStorage.setItem("client_token", result)
        });
    }

     private setCredentials(credentials?: any, remember?: boolean) {


    this._credentials = credentials || null;

    if (credentials) {

      const storage =  localStorage;

      storage.setItem("token",credentials.token);
      if(credentials.caterer_admin)
      {
        storage.setItem("Role","Caterer_Admin");  
      }
      else
      {
       storage.setItem("Role","Caterer_Subadmin");   
      }
      

    } else {

      localStorage.removeItem(this.credentialsKey);

      sessionStorage.removeItem(this.credentialsKey);
    }
  }


  public temp_storage(key,content)
  {
    console.log(key,content)
    const storage =  localStorage;
    storage.setItem(key,content);
  }

setToken(context: any): Observable<Credentials> {
    console.log(context)
    this.setCredentials(context);

    return of(context);
  }

    // public setToken(token) {

    //     this.setCredentials(token)
    //    // localStorage.setItem("Catere_token", token);
    // }
    
    public isAuthenticated(): boolean {        
        const token = localStorage.getItem('token');
        if (token) {
          
           return true;          
        }
        else
        {
            return false
        }
        return !this.jwtHelper.isTokenExpired(token);
        
    }


    public logout() {
        localStorage.clear();

        

    }
}