import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { VendorComponent } from './vendor/vendor.component';
import { AddMenuComponent } from './add-menu/add-menu.component';
import { AuditComponent } from './audit/audit.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { MenuComponent } from './menu/menu.component';
import { MisreportitemComponent } from './misreportitem/misreportitem.component';
import { MisreportorderComponent } from './misreportorder/misreportorder.component';
import { OrdersComponent } from './orders/orders.component';
import { PaymentComponent } from './payment/payment.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import {ForgetpasswordComponent} from './forgetpassword/forgetpassword.component';
import {ChangepasswordComponent} from './changepassword/changepassword.component';

import { HomeCaterarComponent } from './home-caterar/home-caterar.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { HttpClient } from '@angular/common/http';
import { TokenInterceptor } from './token.interceptor';

import { AuthGuardService as AuthGuard } from './auth-guard.service';
import { AuthService } from './auth.service';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';

import { AppSettings } from './app.setting';




const routes: Routes = [
{ path: 'home', component: HomeComponent },
 { path: 'vendor', component:VendorComponent},
 { path: 'add-menu', component:AddMenuComponent},
 { path: 'audit', component:AuditComponent},
 { path: 'confirmation', component:ConfirmationComponent},
 { path: 'menu', component:MenuComponent},
 { path: 'misreportitem', component:MisreportitemComponent},
 { path: 'misreportorder', component:MisreportorderComponent},
 { path: 'orders', component:OrdersComponent},
 { path: 'payment', component:PaymentComponent},
 { path: 'profile', component:ProfileComponent},
 { path: 'dashboard', component:DashboardComponent},
 { path: 'signup', component:SignupComponent},
 { path: 'login', component:LoginComponent},
 { path: 'forgetpassword',component:ForgetpasswordComponent},
 { path: 'changepassword',component:ChangepasswordComponent}, 
 { path: 'home-caterer', component:HomeCaterarComponent},

{ path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
  
   JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token');
        },
        whitelistedDomains: [AppSettings.DOMAIN]
      }
    })   
   ],
  providers: [

     AuthGuard,
     AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },

    HttpClient
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
AppComponent,
HomeComponent,
VendorComponent,
AddMenuComponent,
AuditComponent,
ConfirmationComponent,
MenuComponent,
MisreportitemComponent,
ChangepasswordComponent,
MisreportorderComponent,
OrdersComponent,
ForgetpasswordComponent,
PaymentComponent,
ProfileComponent,
DashboardComponent,
SignupComponent,
LoginComponent,
HomeCaterarComponent
];
