import { Component, OnInit,ViewChild,EventEmitter,Output,Input, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import {
    AuthService
} from '../auth.service';

import Swal from 'sweetalert2'
@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  constructor(private router: Router,private spinnerService: Ng4LoadingSpinnerService, private route: ActivatedRoute, private auth:AuthService,private http: HttpClient,private spinner: NgxSpinnerService) { }
showpopup:boolean = false;
value:any;
rowsFilter:any=[];
catt_id:any;
order_list:any=[];
menu_images:any;

  ngOnInit() {
  this.showpopup = true;
  this.loaddata();
  }

   loaddata()
  {
      let myItem = this.auth.getToken();
     this.value = { "token" : myItem}
     this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'GetCatter_details/', this.value).subscribe((data: any) => {          
          this.rowsFilter = data.data;
          this.catt_id =data.data.catt_id;
          
          this.http.post(AppSettings.API_ENDPOINT + 'GetMyOrder_newList/' , this.value).subscribe((data: any) => {  
          this.spinnerService.hide();        
          this.order_list = data.data;
          this.menu_images = AppSettings.Menu_image;         
          console.log(this.order_list)
        });


     });

  }


Cancel(id)
{
	console.log(id)



Swal({title: "Reason For Cancellation",inputPlaceholder:'Reason For Cancellation', input: 'text', showCancelButton: false,confirmButtonText:'Submit'}).then(result => {

         if (result.value) {
       
      	let data={
        "order_id":id,
        "ord_status":"Cancelled",
        "reason_cancel":result.value
    }
           console.log(result)
           this.spinnerService.show();
           this.http.put(AppSettings.API_ENDPOINT + 'GetMyOrder_List/' +id,{"data":
            data}).subscribe((data: any) => {   
             this.loaddata();           
             this.spinnerService.hide();
           }, error => {
           })
            Swal("Order Cancelled")
         } else {

         }
   })

//  Swal({
//   title: 'Are you sure?',
//   //text: 'You will not be able to recover this imaginary file!',
//   type: 'warning',
//   showCancelButton: true,
//   confirmButtonText: 'Yes!',
//   cancelButtonText: 'No'
// }).then((result) => {
//   if (result.value) {


// Swal({
//   title: "An input!",
//   text: "Write something interesting:",
//   type: "input",
//   showCancelButton: true,
//   closeOnConfirm: false,
//   inputPlaceholder: "Write something"
// }, function (inputValue) {
//   if (inputValue === false) return false;
//   if (inputValue === "") {
//     Swal.showInputError("You need to write something!");
//     return false
//   }
//   Swal("Nice!", "You wrote: " + inputValue, "success");
// });

}

   Order_stats(status,id)
{
    let data={
        "order_id":id,
        "ord_status":status
    }
       this.spinner.show();  
       this.spinnerService.show();
          this.http.put(AppSettings.API_ENDPOINT + 'GetMyOrder_List/' +id,{"data":
            data}).subscribe((data: any) => {          
           this.spinner.show();  
           this.spinnerService.hide();
                if (data.status == "ok") {
                  Swal("Success", " Order Updated", "success");
                  // this.ShowForm = !this.ShowForm;
                  this.loaddata();
                }
          // this.catt_id =data.data.catt_id;
          
          this.menu_images = AppSettings.Menu_image;         

        });


}


}
