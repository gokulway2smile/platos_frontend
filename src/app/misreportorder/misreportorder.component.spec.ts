import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisreportorderComponent } from './misreportorder.component';

describe('MisreportorderComponent', () => {
  let component: MisreportorderComponent;
  let fixture: ComponentFixture<MisreportorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisreportorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisreportorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
