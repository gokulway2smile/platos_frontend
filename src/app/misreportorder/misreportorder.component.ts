import { Component, OnInit,ViewChild, TemplateRef,ViewEncapsulation,ElementRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {
    AuthService
} from '../auth.service';

import Swal from 'sweetalert2';

import {
    NgbModal,
    ModalDismissReasons,
    NgbActiveModal

} from '@ng-bootstrap/ng-bootstrap';

import { AgGridNg2 } from 'ag-grid-angular';



@Component({
  selector: 'app-misreportorder',
  templateUrl: './misreportorder.component.html',
  styleUrls: ['./misreportorder.component.scss']
})





export class MisreportorderComponent implements OnInit {

  ordersreport: any=[];
  value:any;
  rowsFilter:any;
  Search_items:any;

AllMenu_list:any;
total_count:any;
tempFilter:any;

@ViewChild('agGrid') agGrid: AgGridNg2;

    title = 'app';



    columnDefs = [
        {headerName: 'Date', field: 'date', sortable: true, filter: true, checkboxSelection: true },
        {headerName: 'Order No', field: 'order', sortable: true, filter: true },
        {headerName: 'Transaction Detail', field: 'transactdetail', sortable: true, filter: true },
        {headerName: 'Client Name', field: 'clname', sortable: true, filter: true },
        {headerName: 'Order Status', field: 'orderstatus', sortable: true, filter: true },
        {headerName: 'Payment Status', field: 'paymentstatus', sortable: true, filter: true },
        {headerName: 'Price', field: 'price1', sortable: true, filter: true }

    ];

    rowData: any;

  constructor(private modalService: NgbModal,private spinnerService: Ng4LoadingSpinnerService,private router: Router, private auth:AuthService, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) { }


  ngOnInit() {

    let myItem = this.auth.getToken();
     this.value = { "token" : myItem}  
     this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'GetMyMIS_reports/', this.value).subscribe((data: any) => {          
           this.spinnerService.hide();
         this.ordersreport = data.data; 
         this.rowData  =data.data;

         
   
         for (var i = this.ordersreport.length - 1; i >= 0; i--) {

             this.rowData = [{date: this.ordersreport[i].created_at, order:this.ordersreport[i].order_id, transactdetail: this.ordersreport[i].ord_transation_status, clname: this.ordersreport[i].userslist.usr_first_name, orderstatus: this.ordersreport[i].ord_status, paymentstatus: this.ordersreport[i].ord_transation_status, price1: this.ordersreport[i].ord_total_amount}];

         }
     
   
  	// this.rowData = [
  	// {date: '21.02.2018', order:'698754231', transactdetail: 'Lorem Ipsum is simply dummy text of the printing and typeset.', clname: 'Lorem Ipsum', orderstatus: 'Confirmed', paymentstatus: 'Paid', price: 'Rs. 3,500'},
  	// {date: '21.02.2018', order:'698754231', transactdetail: 'Lorem Ipsum is simply dummy text of the printing and typeset.', clname: 'Lorem Ipsum', orderstatus: 'Confirmed', paymentstatus: 'Paid', price: 'Rs. 3,500'},
  	// {date: '21.02.2018', order:'698754231', transactdetail: 'Lorem Ipsum is simply dummy text of the printing and typeset.', clname: 'Lorem Ipsum', orderstatus: 'Confirmed', paymentstatus: 'Paid', price: 'Rs. 3,500'},
  	// {date: '21.02.2018', order:'698754231', transactdetail: 'Lorem Ipsum is simply dummy text of the printing and typeset.', clname: 'Lorem Ipsum', orderstatus: 'Confirmed', paymentstatus: 'Paid', price: 'Rs. 3,500'},
  	// {date: '21.02.2018', order:'698754231', transactdetail: 'Lorem Ipsum is simply dummy text of the printing and typeset.', clname: 'Lorem Ipsum', orderstatus: 'Confirmed', paymentstatus: 'Paid', price: 'Rs. 3,500'},
  	// {date: '21.02.2018', order:'698754231', transactdetail: 'Lorem Ipsum is simply dummy text of the printing and typeset.', clname: 'Lorem Ipsum', orderstatus: 'Confirmed', paymentstatus: 'Paid', price: 'Rs. 3,500'},
  	// {date: '21.02.2018', order:'698754231', transactdetail: 'Lorem Ipsum is simply dummy text of the printing and typeset.', clname: 'Lorem Ipsum', orderstatus: 'Confirmed', paymentstatus: 'Paid', price: 'Rs. 3,500'},
  	// {date: '21.02.2018', order:'698754231', transactdetail: 'Lorem Ipsum is simply dummy text of the printing and typeset.', clname: 'Lorem Ipsum', orderstatus: 'Confirmed', paymentstatus: 'Paid', price: 'Rs. 3,500'},
  
  	// ];


  });
}



// Search()
// {
//   console.log(this.Search_items)

//     this.spinner.show();  
//         this.http.post(AppSettings.API_ENDPOINT + 'getSearch_items/', { 'search': this.Search_items}).subscribe((data: any) => {
//         this.spinner.hide();  
//         this.AllMenu_list= data.data;
//         this.total_count=this.AllMenu_list.length;
//     });


// }


updateFilter(event) {
    // const val = event.target.value.toLowerCase();
    const val = this.Search_items;
    
    this.tempFilter = this.rowData;


    // filter our data
    if(val)
    {
    const temp = this.tempFilter.filter(function(d) {

      return d.date.indexOf(val) !== -1 || d.order.indexOf(val) !== -1|| d.transactdetail.indexOf(val) !== -1|| d.clname.indexOf(val) !== -1|| d.orderstatus.indexOf(val) !== -1|| d.paymentstatus.indexOf(val) !== -1||  !val;
    });

    // update the rows
    this.rowData = temp;
  }
  else
  {
    let myItem = this.auth.getToken();
     this.value = { "token" : myItem}  
     this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'GetMyMIS_reports/', this.value).subscribe((data: any) => {          
           this.spinnerService.hide();
         this.ordersreport = data.data; 
         this.rowData  =data.data;

         
   
         for (var i = this.ordersreport.length - 1; i >= 0; i--) {

             this.rowData = [{date: this.ordersreport[i].created_at, order:this.ordersreport[i].order_id, transactdetail: this.ordersreport[i].ord_transation_status, clname: this.ordersreport[i].userslist.usr_first_name, orderstatus: this.ordersreport[i].ord_status, paymentstatus: this.ordersreport[i].ord_transation_status, price1: this.ordersreport[i].ord_total_amount}];

         }
       })
  }

    // Whenever the filter changes, always go back to the first page
    // this.table.offset = 0;
  }

 // getSelectedRows() {
 //        const selectedNodes = this.agGrid.api.getSelectedNodes();
 //        const selectedData = selectedNodes.map( node => node.data );
 //        const selectedDataStringPresentation = selectedData.map( node => node.make + ' ' + node.model).join(', ');
 //        alert(`Selected nodes: ${selectedDataStringPresentation}`);
 //    }





keyDownFunction(event) {
  if(event.keyCode == 13) {
    this.updateFilter(event)
    // rest of your code
  }
}

}
