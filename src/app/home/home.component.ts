import { Component, OnInit, HostListener } from '@angular/core';
import {Http} from '@angular/http';
import {HttpClient} from '@angular/common/http';
import{ AppSettings } from './../app.setting';
import {ActivatedRoute,Router} from "@angular/router";
import { BrowserModule }    from '@angular/platform-browser';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

 constructor(private router: Router, private spinnerService: Ng4LoadingSpinnerService,private _route: ActivatedRoute,private http: HttpClient) { }

  array1 :any=[];
  imagesUrl :any=[];
  testimonialwids:any;
  testimonial : any=[];
  twocount: number;
  styletest: any;
  testimon: any;
  next: any;
  prev: any;
  valrange:any;
  temptesti:any = 0;
  temptestire:any = 0;
  lenfind:any;
  testiarray = [];
  	@HostListener('window:scroll', ['$event']) 
    scrollHandler(event) {
      console.debug("Scroll Even asdfsdft");
      return;
    }

  ngOnInit() {
    
  	 this.array1 = ['/assets/images/oyo.png', '/assets/images/zapier.png', 'assets/images/unicef.png', '/assets/images/drupal.png'];

     this.imagesUrl = [
     '/assets/images/oyo.png',
     '/assets/images/zapier.png',
     '/assets/images/unicef.png',
    // {image:'/assets/images/unicef.png', words:'test page test page test page test page'}
    ];

      this.spinnerService.show();
     this.http.get(AppSettings.API_ENDPOINT + 'getall_testimonial/').subscribe((data: any) => {
       this.spinnerService.hide();
         this.testiarray = data.data;
         this.lenfind = data.data.length;
        if(this.lenfind > 0)
        {
            var testimonialwids = this.lenfind * 530;
            this.testimon = testimonialwids;
            this.twocount = 1060;
             this.styletest = {
                           'width' : testimonialwids + 'px',
                       }
        }
     }
     );
  }



Caterers_page()
{
  this.router.navigate(['home-caterer']);
  window.location.reload();
}

prevtti(){
// this.temptesti = this.twocount + this.temptestiprev;
this.testislider("prevt", this.temptesti);
}
nextti(){
  // this.temptesti = this.twocount + this.temptesti;
  this.testislider("nextt", this.temptesti);
}

  testislider(identify, val){ 
    if(identify == 'prevt')
    {
      if(this.temptesti === 0)
        {
          return;
        } 

      this.valrange = '+';
      this.temptesti = this.temptesti + this.twocount - this.temptesti;
      this.temptesti = this.temptesti - this.temptestire;
      if(this.temptesti < 0)
      {
        console.log("before add "+this.temptesti);
        this.temptesti = -this.temptesti;
        this.valrange = '-';
      }
    }
    if(identify == 'nextt')
    {
      console.log("find the testival "+this.temptesti);
     this.valrange = '-';
     this.temptesti = this.twocount + this.temptesti;
     this.temptesti = this.temptesti + this.twocount - this.temptesti;
      this.temptesti = this.temptesti + this.temptestire;
      if(this.temptesti >= this.testimon)
      {
        console.log("finish this task");
        return;
      }
    }
 
 let testwid = this.testimon;
this.temptestire = this.temptesti;
  this.styletest = {
               'width': testwid + 'px',
               // 'margin-left': '-'+this.next+'px',
               'margin-left': this.valrange+this.temptesti+'px',
           }
}



//  nextslide(){ 
//    console.log(this.prev);
//  this.temptesti = this.twocount + this.temptesti;
//  this.next = this.temptesti;  
//  let testwid = this.testimon;
//  console.log(testwid);
//  if(testwid > this.next){ 
//   this.styletest = {
//                'width': testwid + 'px',
//                'margin-left': '-'+this.next+'px',
//            }
//          }
// }

//  prevtch(){  
//  this.temptesti = this.twocount + this.temptestiprev;
//  this.prev = this.temptesti - this.next;  
//  let testwidprev = this.testimon;
//  if(testwidprev > this.prev){  
//    this.styletest = {
//                'width': testwidprev + 'px',
//                'margin-left': '+'+this.prev+'px',
//            }
//          }
// }



}

