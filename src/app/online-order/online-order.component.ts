import { Component, OnInit } from '@angular/core';
import { NgxCarousel } from 'ngx-carousel';

@Component({
  selector: 'app-online-order',
  templateUrl: './online-order.component.html',
  styleUrls: ['./online-order.component.scss']
})
export class OnlineOrderComponent implements OnInit {
  constructor() { }
 public myfunc(event: Event) {
    
  }
  public carouselOnetest: NgxCarousel;	
  incval:number = 10;
tempval:number = 0;

placelist:any = [
{dish: 'MEETING PLATE CLASSIC', rupee: 'Rs. 4000', incval: 10 },
{dish: 'MEETING PLATE CLASSIC', rupee: 'Rs. 4000', incval: 10 },
{dish: 'MEETING PLATE CLASSIC', rupee: 'Rs. 4000', incval: 10 },
{dish: 'MEETING PLATE CLASSIC', rupee: 'Rs. 4000', incval: 10 },
];



  ngOnInit() {


  	this.carouselOnetest = {
      grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
      slide: 3,
      speed: 400,
      interval: 4000,
      point: {
        visible: true
      },
      load: 2,
      touch: true,
      loop: true,
      custom: 'banner'
    }


  }



  incfn(pl, ti){

  	if(pl == 'plus')
  	{
  	this.placelist[ti].incval	= this.placelist[ti].incval + 1;
  	}
  	if(pl == 'minus')
  	{
  		if(this.placelist[ti].incval > 0)
  		{ 
  		this.placelist[ti].incval = this.placelist[ti].incval - 1;
  		}
  	}
  
  }

}
