import { Component, OnInit,ViewChild,EventEmitter,Output,Input, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import {
    AuthService
} from '../auth.service';

import Swal from 'sweetalert2'

import {
    NgbModal,
    ModalDismissReasons,
    NgbActiveModal

} from '@ng-bootstrap/ng-bootstrap';

import { AgGridNg2 } from 'ag-grid-angular';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
 ordersreport: any=[];
  value:any;
  rowsFilter:any;
  Search_items:any;

AllMenu_list:any;
total_count:any;
tempFilter:any;

@ViewChild('agGrid') agGrid: AgGridNg2;

    title = 'app';



    columnDefs = [
        {headerName: 'Order No', field: 'Order', sortable: true, filter: true, checkboxSelection: true },
        {headerName: 'Payment Status', field: 'paymentstatus', sortable: true, filter: true },
        {headerName: 'Payment Date', field: 'paydate', sortable: true, filter: true },
        {headerName: 'Transaction Detail', field: 'transactdetail', sortable: true, filter: true },
        {headerName: 'Cheque Image', field: 'cheque', sortable: true, filter: true }       
    	];

    rowData: any;

  constructor(private modalService: NgbModal,private router: Router,private spinnerService: Ng4LoadingSpinnerService, private auth:AuthService, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) { }


  ngOnInit() {

  	 let myItem = this.auth.getToken();
     this.value = { "token" : myItem}  
     this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'GetMyMIS_reports/', this.value).subscribe((data: any) => {          
           this.spinnerService.hide();
         this.ordersreport = data.data; 
         this.rowData  =data.data;

         
   	console.log(this.ordersreport)
         for (var i = this.ordersreport.length - 1; i >= 0; i--) {

             this.rowData = [{Order: this.ordersreport[i].order_id, paymentstatus:this.ordersreport[i].ord_status, paydate: this.ordersreport[i].updated_at,  transactdetail: this.ordersreport[i].ord_transation_status, cheque: ""}];

         }
     
     })
  }


updateFilter(event) {
    // const val = event.target.value.toLowerCase();
    const val = this.Search_items;
    
    this.tempFilter = this.rowData;


    // filter our data
    if(val)
    {
    const temp = this.tempFilter.filter(function(d) {

      return d.Order.indexOf(val) !== -1 || d.paymentstatus.indexOf(val) !== -1|| d.paydate.indexOf(val) !== -1|| d.transactdetail.indexOf(val) !== -1||  !val;
    });

    // update the rows
    this.rowData = temp;
  }
  else
  {
    
    this.ngOnInit();
  }

    // Whenever the filter changes, always go back to the first page
    // this.table.offset = 0;
  }

 // getSelectedRows() {
 //        const selectedNodes = this.agGrid.api.getSelectedNodes();
 //        const selectedData = selectedNodes.map( node => node.data );
 //        const selectedDataStringPresentation = selectedData.map( node => node.make + ' ' + node.model).join(', ');
 //        alert(`Selected nodes: ${selectedDataStringPresentation}`);
 //    }





keyDownFunction(event) {
  if(event.keyCode == 13) {
    this.updateFilter(event)
    // rest of your code
  }
}


}
