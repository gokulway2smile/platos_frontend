import {   Component, OnInit,ViewChild, TemplateRef,ViewEncapsulation,ElementRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import {transition, trigger, style, animate} from '@angular/animations';
import swal from 'sweetalert2';
import { VendorService } from './vendor.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import {
    AuthService
} from '../auth.service';
  
import Swal from 'sweetalert2';

import {
    NgbModal,
    ModalDismissReasons,
    NgbActiveModal

} from '@ng-bootstrap/ng-bootstrap';


import {NgbCalendar,NgbDate, NgbDateParserFormatter, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

import { ToastrService } from 'ngx-toastr';

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;


@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.scss']  
})
 
export class VendorComponent implements OnInit {
 

public show:boolean = false;
  public buttonName:any = 'Show';

closeResult: string;
tablelist:any=[];
user:any=[];
certificate:any=[];
catemenu:any=[];
cinfo:any=[];
address:any=[];
rowsFilter:any;
food_certificate:any;
foodcertificate:any;
gst_certificate:any;
gstcertificate:any;
Addresss:any=[];
selectgroup:any;

AddUser:any[];
FirstName:any;
SurName:any;
dropdownList:any=[];
selectedItems1:any=[];
dropdownSettings1:any;
EmailAddress:any;
PhoneNumber:any;
DeliveryFromTime:any=[];
DeliveryToTime:any=[];
EventType:any;
MinOrder:any;
MaxOrder:any;
LeadTime:any;
Score:any;
GstCertificate:any;
FoodCertificate:any;
FileUpload:any;
Name:any;
Password:any;
Email:any;
Phone:any;
catt_id:any;
catt_first_name:any;
result:any;
error:any=[];
myFiles:any=[];
caterersub_profile:any;
myFiles1:any=[];
Menu_list:any=[];
AllMenu_list:any=[];
Menu_group:any=[];
MenuType:any=[];
profile_pic:any=[];
MenuName:any;
is_upload:boolean=false;
fileName:any;
filePath:any;
fileData:any=[];
fileName1:any;
profile:any;
filePath1:any;
fileData1:any=[];
GST_image:any;
selecth:boolean = true;
selected:boolean=true;
is_edit:boolean =false;
special_category:any=[];
additional_user:any={};
CategoryName:any;
caterer_profile:any;
addprofile_pic:any=[];
hoveredDate: NgbDate;
fromDate: NgbDate;
toDate: NgbDate;

addprofile:any;

Address = [{
      address:"",
      pincode:"",
    }];

    catt_add_users = [{
      addi_cat_id:"",
      name:"",
      password:"",
      email:"",
      phone:"",
    }];

Food_image = AppSettings.Food_certificate;

 value:any;
 showDialog = false;
 Is_Admin:any;
 public config: any;

 constructor(public toastr: ToastrService,private modalService: NgbModal,private spinnerService: Ng4LoadingSpinnerService,private router: Router, private auth:AuthService, private _route: ActivatedRoute,private http: HttpClient,public parserFormatter: NgbDateParserFormatter, public calendar: NgbCalendar,private spinner: NgxSpinnerService) { 

    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday());
  }

  ngOnInit() {

    let Role = this.auth.CheckRole();
         if(Role =='Caterer_Admin')
         {
           this.Is_Admin = true;
         }
         else
         {
           this.Is_Admin =false;
         }

    let myItem = this.auth.getToken();
     this.value = { "token" : myItem}   
     this.spinnerService.show();
     this.http.post(AppSettings.API_ENDPOINT + 'getcat_grouplist/',this.value).subscribe((data: any) => {
     this.spinnerService.hide();
     this.Menu_group = data.data;   
     console.log(this.Menu_group);  

 
       this.spinnerService.show();
       this.http.get(AppSettings.API_ENDPOINT + 'getall_groups/').subscribe((data: any) => {
       this.spinnerService.hide();
       this.Menu_list = data.data;
        
      }, error => {})    
     
        this.spinnerService.show();
        this.http.post(AppSettings.API_ENDPOINT + 'GetAll_catterMenu/', this.value).subscribe((data: any) => {
        this.spinnerService.hide();
        this.AllMenu_list= data.data;         
        this.MenuName = this.AllMenu_list.menu_name;        
        let dropdown:any=[];
        for (var i = data.data.length - 1; i >= 0; i--) {
          dropdown.push({"menu_id":data.data[i].menu_id,"menu_name":data.data[i].menu_name})

        }
         this.dropdownList = dropdown;

              this.dropdownSettings1 = {
              singleSelection: false,
              idField: 'menu_id',
              textField: 'menu_name',
              selectAllText: 'Select All',
              unSelectAllText: 'UnSelect All',
              itemsShowLimit: 5,
              allowSearchFilter: true
            };

            }, error => {})


    })

         this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'GetCatter_details/', this.value).subscribe((data: any) => {          
         this.spinnerService.hide();
         this.rowsFilter = data.data;
         this.catt_id = this.rowsFilter.catt_id;
         this.FirstName = this.rowsFilter.catt_first_name;
         this.SurName = this.rowsFilter.catt_sur_name;
         this.EmailAddress = this.rowsFilter.catt_email_address;
         this.PhoneNumber = this.rowsFilter.catt_mobile_no;
         this.DeliveryFromTime = this.rowsFilter.catt_delivery_fromtime;
         this.DeliveryToTime = this.rowsFilter.catt_delivery_totime;
         this.EventType = this.rowsFilter.catt_event_type;
         this.MinOrder = this.rowsFilter.catt_min_order;
         this.MaxOrder = this.rowsFilter.catt_max_order;
         this.LeadTime = this.rowsFilter.catt_lead_time;
         this.GstCertificate = this.rowsFilter.catt_gst_certificate;
         this.FoodCertificate = this.rowsFilter.catt_food_certificate;
         // this.FileUpload = this.rowsFilter.catt_lead_time;
         this.Score = this.rowsFilter.catt_score;
         this.selectgroup = this.rowsFilter.catt__group

         this.special_category =this.rowsFilter.special_category;
         if(this.rowsFilter.addi_user)
         {
           this.additional_user = this.rowsFilter.addi_user;  
           this.caterersub_profile = AppSettings.Profile_pic + this.additional_user.cat_usr_profile_pic;
           console.log(this.additional_user)
         }
         else
         {
        
              this.additional_user.cat_usr_name =""
              this.additional_user.cat_usr_email =""
              this.additional_user.cat_usr_phone =""
              this.additional_user.cat_usr_password =""
              this.additional_user.cat_usr_id =""

         }
         
          this.user = [
          {name: this.rowsFilter.catt_first_name, email: this.rowsFilter.catt_email_address, phone: this.rowsFilter.catt_mobile_no },

          ];

          this.Addresss =this.rowsFilter.cust;
          console.log(this.Addresss)
          this.AddUser =this.catt_add_users;
          this.food_certificate = this.rowsFilter.catt_food_certificate;
          this.gst_certificate = this.rowsFilter.catt_gst_certificate;
          this.foodcertificate = AppSettings.Food_certificate  + this.rowsFilter.catt_food_certificate;
          this.gstcertificate = AppSettings.GST_certificate + this.rowsFilter.catt_gst_certificate;

          this.caterer_profile = AppSettings.Profile_pic + this.rowsFilter.catt_profile_pic;

          this.cinfo=[
          {
          deliverydate: '10.01.2019',
          deliverytime: this.rowsFilter.catt_delivery_fromtime + " : " + this.rowsFilter.catt_delivery_totime,
          leadtime: this.rowsFilter.catt_lead_time +  ' days',
          minorder: this.rowsFilter.catt_min_order,
          maxorder: this.rowsFilter.catt_max_order},

          ];
          this.catemenu = [this.Menu_list];
          });

}


// isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate);
  // isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);

getFileDetails (e) {
  //console.log (e.target.files);
  for (var i = 0; i < e.target.files.length; i++) {
    this.myFiles.push(e.target.files[i]);
    this.is_upload=true;
  }
}

getFileDetails1 (e) {
  //console.log (e.target.files);
  for (var i = 0; i < e.target.files.length; i++) {
    this.myFiles1.push(e.target.files[i]);
    this.is_upload=true;
  }
}



profilepic(e) {
  //console.log (e.target.files);
  for (var i = 0; i < e.target.files.length; i++) {
    this.profile_pic.push(e.target.files[i]);
  }
}

addprofilepics(e){
 //console.log (e.target.files);
  for (var i = 0; i < e.target.files.length; i++) {
    this.addprofile_pic.push(e.target.files[i]);
  }
}

   async downloadImages(frmData,is_go) {
   return new Promise((resolve, reject) => {
  
let data = [];
let temp_data:any;
this.fileName = null;
this.filePath = null;        
this.spinnerService.show();
 data.push(new Promise((resolve, reject) => {
resolve( this.http.post(AppSettings.API_ENDPOINT + 'catgst_image/', frmData).subscribe((temp_data: any) => {
  this.spinnerService.hide();
                    this.fileData.push(temp_data.data);
                    console.log(this.fileData)
                    this.fileName = this.fileData[0]['gstpic'];
                    this.filePath = this.fileData.file_path 
                    if(!is_go)
                     this.condtionCheck()  
          }))
      }))
      Promise.all(data).then((values) => {
          resolve(values)
      });
  })

}

async Profile_pic(frmData2) {
   return new Promise((resolve, reject) => {
  
let data = [];
let temp_data:any;
this.fileName = null;
this.filePath = null;        
this.spinnerService.show();
 data.push(new Promise((resolve, reject) => {
this.http.post(AppSettings.API_ENDPOINT + 'catt_profile/', frmData2).subscribe(async(temp_data: any) => {
  this.spinnerService.hide();
                    this.fileData.push(temp_data.data);
                    this.fileName = this.fileData[0].profile_pic;
                    this.profile = this.fileName;
                    this.condtionCheck()  
          })
      }))
      Promise.all(data).then((values) => {
          resolve(values)
      });
  })
}

addprofilepic(frmData2) 
{
   return new Promise((resolve, reject) => {
  
let data = [];
let temp_data:any;
this.fileName = null;
this.filePath = null;        
this.spinnerService.show();
 data.push(new Promise((resolve, reject) => {
this.http.post(AppSettings.API_ENDPOINT + 'catt_profile/', frmData2).subscribe(async(temp_data: any) => {
  this.spinnerService.hide();
                    this.fileData.push(temp_data.data);
                    this.fileName = this.fileData[0].profile_pic;
                    this.addprofile = this.fileName;

                    this.AddUser = [{
                addi_cat_id:this.additional_user.cat_usr_id,
                name:this.additional_user.cat_usr_name,
                password:this.additional_user.cat_usr_password,
                email:this.additional_user.cat_usr_email,
                phone:this.additional_user.cat_usr_mobile,
                addprofile :this.addprofile
              }];
                    this.condtionCheck()  
          })
      }))
      Promise.all(data).then((values) => {
          resolve(values)
      });
  })
}




async downloadImages2(frmData1) {
   return new Promise((resolve, reject) => {
  
let data = [];
               let temp_data1:any;
                this.fileName1 = null;
                this.filePath1 = null;        
this.spinnerService.show();
 data.push(new Promise((resolve, reject) => {
resolve( this.http.post(AppSettings.API_ENDPOINT + 'catfood_image/', frmData1).subscribe(async(temp_data1: any) => {
  this.spinnerService.hide();
                this.fileData1 = temp_data1.data;
                this.fileName1 = this.fileData1.foodpic;
                this.filePath1 = this.fileData1.file_path;
                    this.condtionCheck()  
          }))
      }))
      Promise.all(data).then((values) => {
          resolve(values)
      });
  })
}

 async onSubmit()
     {
       this.spinnerService.show();
       this.error = "";   

       const frmData = new FormData();
           if(this.myFiles.length>0)
           {
           for (var i = 0; i < this.myFiles.length; i++) {
             frmData.append("gstpic", this.myFiles[i]);
           }
           }

            const frmData1 = new FormData();
            if(this.myFiles)
            {
             if(this.myFiles1.length>0)
               {

               for (var i = 0; i < this.myFiles1.length; i++) {
                 frmData1.append("foodpic", this.myFiles1[i]);
               }
               }
           }

           const frmData2 = new FormData();
            if(this.profile_pic.length>0)
           {
           for (var i = 0; i < this.profile_pic.length; i++) {
             frmData2.append("profile_pic", this.profile_pic[i]);
           }
           }

           if(this.addprofile_pic.length>0)
           {
           for (var i = 0; i < this.addprofile_pic.length; i++) {
             frmData2.append("profile_pic", this.addprofile_pic[i]);
           }
           }


           if(this.profile_pic.length>0)
                {
                   await this.Profile_pic(frmData2);                   
                 }


                 if(this.addprofile_pic.length>0)
                {
                   await this.addprofilepic(frmData2);                   
                 }

             this.AddUser = [{
                addi_cat_id:this.additional_user.cat_usr_id,
                name:this.additional_user.cat_usr_name,
                password:this.additional_user.cat_usr_password,
                email:this.additional_user.cat_usr_email,
                phone:this.additional_user.cat_usr_mobile,
                addprofile :this.addprofile
              }];
              console.log(this.AddUser);
                if(this.myFiles.length>0)
                {

                  if(this.myFiles1.length>0)
                    {
                    await this.downloadImages(frmData,true);    
                    }
                    else
                    {
                    await this.downloadImages(frmData,false);    
                    }
                  
                    }


 
                if(this.myFiles1.length>0)
                {
                  await this.downloadImages2(frmData1);               
                }
                
               
                let data1 = {
                    "catt_first_name": this.FirstName,
                    "catt_email_address": this.EmailAddress,
                    "catt_mobile_no": this.PhoneNumber,
                    "catt_sur_name": this.SurName,
                    "FromDate": this.fromDate,
                    
                    "ToDate": this.toDate,
                    "DeliveryFromTime": this.DeliveryFromTime,
                    "DeliveryToTime": this.DeliveryToTime,
                    "catt_event_type": this.EventType,
                    "catt_min_order": this.MinOrder,
                    "catt_max_order": this.MaxOrder,
                    "catt_lead_time": this.LeadTime,
                    "catt_score": this.Score,
                    "catt_add_users": this.AddUser,
                    "catt_address": this.Addresss,
                    "catt_id":this.catt_id,
                    "caterer_group" : this.selectedItems1
                }   

            
              if(this.catt_id)
                  {  

                      if(this.is_upload)
                      {
                    await    this.condtionCheck()
                      }                      
                      else
                      {
                        console.log(data1)
                        this.update(data1,this.catt_id);
                      }
                  }
                  this.spinnerService.hide();
        }



        condtionCheck(){         
          
                  let fromtime= this.DeliveryFromTime;
                  let totime= this.DeliveryToTime;
                  let data1 = {
                "catt_first_name": this.FirstName,
                "catt_email_address": this.EmailAddress,
                "catt_mobile_no": this.PhoneNumber,
                "catt_sur_name": this.SurName,
                "catt_delivery_fromtime": fromtime,
                "catt_delivery_totime": totime,
                "catt_event_type": this.EventType,
                "catt_min_order": this.MinOrder,
                "catt_profile_pic":this.profile,
                "catt_max_order": this.MaxOrder,
                "catt_lead_time": this.LeadTime,
                "catt_score": this.Score,
                "catt_gst_certificate": this.fileName,
                "catt_food_certificate": this.fileName1,
                "catt_add_users": this.AddUser,
                "catt_address": this.Addresss,
                "catt_id":this.catt_id,
                "caterer_group" : this.selectedItems1
            } 
            
            this.spinnerService.show();
             setTimeout(()=> this.update(data1,this.catt_id),2000)
            this.spinnerService.hide();

      }

   update(data1,id)
    {
       let myItem = this.auth.getToken();
     this.value = { "token" : myItem}
        this.spinnerService.show();
        this.http.put(AppSettings.API_ENDPOINT + 'getall_catterlist/' + myItem, {
                "data": data1,  
            }).subscribe((data: any) => {
              this.spinnerService.show();
        
                if (data.status == "ok") {
                    this.toastr.success("","Vendor Updated");
                    this.modalService.dismissAll();
                    this.ngOnInit();                                       
                   return;
                } else {
                  this.toastr.warning("",data.data);                
            }
            }, error => {})
        
    }


    onMenuSubmit()
     {
       this.error = "";   
          let myItem = this.auth.getToken();
           this.value = { "token" : myItem}
                let data2 = {
                    "group_name": this.MenuName,
                    "token" : myItem
                   
                }
                this.addnew(data2);
        }

     onCatMenuSubmit()
     {
       
       this.error = "";   
                let data3 = {
                    "menugroup_id": this.MenuType,
                    "menu_list": this.selectedItems1,
                }
                this.addmenu(data3);
        }

   onCatSubmit()
     {
       this.error = "";   
          let myItem = this.auth.getToken();
           this.value = { "token" : myItem}
                let data4 = {
                    "category_name": this.CategoryName,
                    "token" : myItem
                   
                }
                this.addsplcat(data4);
        }


 addsplcat(data4)
        {
          let myItem = this.auth.getToken();
           this.value = { "token" : myItem}
            this.spinnerService.show();
            this.http.post(AppSettings.API_ENDPOINT + 'getmy_specialcategory/' , {
                    "data": data4,
                }).subscribe((data: any) => {
            this.spinnerService.hide();
                    if (data.status == "ok") {
                      this.toastr.success("","Special Category Added");
                      this.modalService.dismissAll();
                      this.ngOnInit();
                    return
                    } else {
                      this.toastr.warning("",data.data);
                }
                }, error => {})
            
    }

  addnew(data2)
      {
        this.spinnerService.show();
        this.http.post(AppSettings.API_ENDPOINT + 'getall_groups/', {
                "data": data2,
            }).subscribe((data: any) => {
        this.spinnerService.hide();
                if (data.status == "ok") {
                    this.toastr.success("","Menugroup Added");
                    this.modalService.dismissAll();
                    this.ngOnInit();
                  return;
                } else {
                  this.toastr.warning("",data.data);
            }
            }, error => {})
        
    }


    addmenu(data3)
        {
          let myItem = this.auth.getToken();
           this.value = { "token" : myItem}
            this.spinnerService.show();
            this.http.put(AppSettings.API_ENDPOINT + 'Set_MenuCategory/' + myItem, {
                    "data": data3,
                }).subscribe((data: any) => {
            this.spinnerService.hide();
                    if (data.status == "ok") {
                      this.toastr.success("","Menugroup Added");
                      this.modalService.dismissAll();
                      this.ngOnInit();
                    return
                    } else {
                      this.toastr.warning("",data.data);
                }
                }, error => {})
            
    }

 modal: any = undefined;


open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

 
  Add() {
    
    this.show = !this.show;
    // CHANGE THE NAME OF THE BUTTON.
    if(this.show)  
      this.buttonName = "Hide";
    else
      this.buttonName = "Show";
  }

  menu_toggle() {

    this.show = !this.show;
    this.selecth = !this.selecth;
  }

  cat_toggle() {
    
    this.show = !this.show;
    this.selected = !this.selected;
    // CHANGE THE NAME OF THE BUTTON.
    if(this.show)  
      this.buttonName = "Hide";
    else
      this.buttonName = "Show";
  }

addItem()
  {
    
    this.Addresss.push({
      address:"",
      pincode :""
    });
  
  }

  deleteFieldValue(index) {
    if(this.Addresss.length>1)
    {
       this.Addresss.splice(index, 1);     
    }
   
 }

 onChange1(Menu_list) {
    
    let myItem = this.auth.getToken();
    let data3 = {
        "token": myItem,
        "menugroup_id"  : this.MenuType
    }

    
     this.spinnerService.show();
            this.http.post(AppSettings.API_ENDPOINT + 'Get_MyGroupmenus/', {
                    "data": data3,
                }).subscribe((data: any) => {

            this.spinnerService.hide();

    for (var i = data.data.length - 1; i >= 0; i--) {
   
  this.selectedItems1.push({ menu_id: data.data[i].menu.menu_id, menu_name: data.data[i].menu.menu_name});                
    }
})        
}



onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

   isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

   isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

}
