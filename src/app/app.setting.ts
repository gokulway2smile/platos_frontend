export class AppSettings {
   public static domain  = "http://localhost:8000";
   public static API_ENDPOINT='http://13.234.1.45/api/';
   public static DOMAIN='http://localhost:8000';
   public static MEDIA='http://localhost:8000/media/';
   public static Image_Base = 'http://13.234.1.45/images/'
   public static TESTIMONIAL_IMAGE='http://13.234.1.45/images/testimonial/';
   public static Food_certificate='http://13.234.1.45/images/caterer/foodimage/';
   public static GST_certificate='http://13.234.1.45/images/caterer/gstimage/';
   public static Menu_image='http://13.234.1.45/images/menu_images/';
   public static Audit_file='http://13.234.1.45/images/caterer/fileimage/';
   public static NO_Image = 'http://13.234.1.45/images/no_image.jpg' 
   public static Profile_pic ='http://13.234.1.45/images/caterer/profile_pic/'
}