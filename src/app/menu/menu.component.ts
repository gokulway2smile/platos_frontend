import { Component, OnInit,ViewChild, TemplateRef } from '@angular/core';
import {FormControl,FormGroup, FormBuilder,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {ActivatedRoute,Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AppSettings } from '../app.setting';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import {
    AuthService
} from '../auth.service';

import { ToastrService } from 'ngx-toastr';


import Swal from 'sweetalert2'

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit { 

catt_id:any;
value:any;
total_count:any;
AllMenu_list:any; 
Search_items:any;
is_menu_added:any;
tempFilter:any;
menu_images:any;
Menu_image:any;
rowsFilter:any;
NO_Image:any

  constructor(public toastr: ToastrService,private router: Router, private spinnerService: Ng4LoadingSpinnerService,private auth:AuthService, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) { }

  ngOnInit() {
  	this.loaddata();
  }


Search()
{
	console.log(this.Search_items)

		this.spinner.show();  
    this.spinnerService.show();
        this.http.post(AppSettings.API_ENDPOINT + 'getSearch_items/', { 'search': this.Search_items}).subscribe((data: any) => {
        this.spinner.hide();  
        this.spinnerService.hide();
        this.AllMenu_list= data.data;
        this.total_count=this.AllMenu_list.length;
    });


}



updateFilter(event) {
    // const val = event.target.value.toLowerCase();
    const val = this.Search_items.toLowerCase();
    
    this.tempFilter = this.AllMenu_list;

    // filter our data
    if(val)
    {
    const temp = this.tempFilter.filter(function(d) {

      return d.menu_name.toLowerCase().indexOf(val) !== -1 || d.cuisint_type.cuis_name.toLowerCase().indexOf(val) !== -1|| !val;
    });

    // update the rows
    this.AllMenu_list = temp;
	}
	else
	{
		this.loaddata();
	}

    // Whenever the filter changes, always go back to the first page
    // this.table.offset = 0;
  }

  goLive()
  {
    let myItem = this.auth.getToken();
     this.value = { "token" : myItem}   
     this.spinnerService.show();
     this.http.post(AppSettings.API_ENDPOINT + 'getmenu_golive/',this.value).subscribe((data: any) => {
     this.spinnerService.hide();
     if (data.status == "ok") {
        this.toastr.success("", "Menu Go Live After Admin Verified");
        this.loaddata();                
        return;
      } else {
      this.toastr.warning("", data.data);
      
  }


   })
  }


edit_menu(menu_name,caterer_name,id)
{
	 let navigationExtras = {
            queryParams: {
                "menu_name": menu_name,
                "caterer": caterer_name,
                "value": id
                
            }
        };

	this.router.navigate(['/add-menu/'],navigationExtras)
}
delete(id)
{

    Swal({
  title: 'Are you sure?',
  //text: 'You will not be able to recover this imaginary file!',
  type: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes!',
  cancelButtonText: 'No'
}).then((result) => {
  if (result.value) {
      this.spinnerService.show();
      this.http.delete(AppSettings.API_ENDPOINT + 'getAll_menu/' +id).subscribe((data: any) => {
           if (data.status == "ok") {  
   this.loaddata();       
   this.spinnerService.hide();
    this.toastr.success("","Menu Deleted")
     }
})
   } else if (result.dismiss === Swal.DismissReason.cancel) {
    
  }
})
this.loaddata();
}


loaddata()
{

	 let myItem = this.auth.getToken();
		 this.value = { "token" : myItem}
		
     console.log(this.rowsFilter)
     this.spinnerService.show();
         this.http.post(AppSettings.API_ENDPOINT + 'GetCatter_details/', this.value).subscribe((data: any) => {          
         // this.rowsFilter = data.data;    
         this.spinnerService.hide();
         this.catt_id =data.data.catt_id;
         this.is_menu_added = data.data.is_menu_added;
        
       
        this.spinnerService.show();
        this.http.post(AppSettings.API_ENDPOINT + 'GetAll_catterMenu/', this.value).subscribe((data: any) => {
        this.spinnerService.hide();
        this.AllMenu_list= data.data;
        this.total_count=this.AllMenu_list.length;
        this.menu_images = data.data;

        this.Menu_image = AppSettings.Menu_image;
        this.NO_Image = AppSettings.NO_Image;

              console.log(this.menu_images);
            }, error => {})



     })

}



}
